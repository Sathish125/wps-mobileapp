import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

  constructor(

    private router: Router,
    private http: HttpClient,

  ) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
        this.user = this.userSubject.asObservable();
        
        // AUTO-LOGOUT
        // this.autoLogout();
        // this.initListener();
        // this.initInterval();
   }

  public get userValue(): User {
    return this.userSubject.value;
}

  login(email, password) {
    console.log("this is user value",this.userSubject.value);

    return this.http.post<any>(`${environment.apiUrl}/auth/loginWithOTP`, { email: email,password: password })
        .pipe(map(responseData => {
            // console.log("This is User Details",responseData);
            if(responseData.success) {
                return responseData;
            }                
            // store user details and jwt token in local storage to keep user logged in between page refreshes
        }));
}

loginWithOTP(email,password,otp){

  return this.http.post<any>(`${environment.apiUrl}/auth/loginWithOTP`, { email: email, password: password, otp: otp })
  .pipe(map(responseData => {
      // console.log(responseData);
      if(responseData.success) {
          var sessionUser: User = {
              id: responseData.user._id,
              first_name: responseData.user.first_name,
              last_name: responseData.user.last_name,
              email: responseData.user.email,
              token: responseData.token,
              account_type: responseData.user.account_type
          };
          localStorage.setItem('user', JSON.stringify(sessionUser));
          this.userSubject.next(sessionUser);
          console.log("this is user value",this.userSubject.value);
          return responseData;
      }
  }));
}
logout(){
   // remove user from local storage and set current user to null
  //  let localData=localStorage.getItem("user");
  //  var localData1=JSON.parse(localData);

  localStorage.removeItem('user');
  let localData=localStorage.getItem("user");
  var localData1=JSON.parse(localData);
  //  console.log("this is myy session work",localData1);
   this.userSubject.next(null);
  //  console.log("in my oen definition",this.userSubject.value);
   //this.router.navigate(['/account/login']);
  //  this.location.replaceState('/'); // clears browser history so they can't navigate with back button
 this.router.navigateByUrl("login");
}

}
