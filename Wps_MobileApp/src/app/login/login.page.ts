import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  otpForm:FormGroup
  form:FormGroup
  hideOTP:boolean = true;
  loading = false;
  loading_OTP = false;
  ErrorMsg = false;
  submitted:boolean = false;
  otpSubmitted = false;
  Incorrect_login = false;
  Incorrect_login_OTP = false;
  returnUrl: string;

  passwordType: string = 'password';
 passwordIcon: string = 'eye-off';

  constructor(
    private formBuilder:FormBuilder,
    private route: ActivatedRoute,
    private router:Router,
    private authService:AuthService,
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

    this.otpForm = this.formBuilder.group({
      otp: ['', Validators.required]
    });

     // get return url from route parameters or default to '/'
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'projects';
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }
  get otpf() { return this.otpForm.controls; }

  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
        return;
    }
    // this.loading = !isResendOTP;
    this.authService.login(this.f.username.value, this.f.password.value)
        .pipe(first())
        .subscribe(
            data => {
                console.log("This  is Successfully OTP received",data);
                this.hideOTP =false
                this.loading= true;
            },
            error => {
                console.log("Incorrect password or username");
                this.Incorrect_login = true;
                this.loading = false;
            });
}

  onOTPSubmit() {
    this.otpSubmitted = true;       
    if (this.otpForm.invalid) {
        return;
    }
    this.loading = true;
    this.authService.loginWithOTP(this.f.username.value, this.f.password.value, this.otpf.otp.value)
        .pipe(first())
        .subscribe(
            data => {
                this.hideOTP = this.loading = true;
                this.router.navigateByUrl(this.returnUrl);
            },
            error => {
                this.Incorrect_login_OTP = true;
                this.loading_OTP = false;
            });
}

error_empty(){
  this.Incorrect_login_OTP = false;
  this.Incorrect_login = false;
}

hideShowPassword() {
  this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
  this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
}

}
