import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
// import { ToastrService } from 'node_modules/process-nextick-args';
import { AuthService } from '../app/services/auth.service';
import { ProjectService } from '../app/services/project.service';
import { CommonService } from 'src/app/services/common.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  loading = false;
  login:any ="";
  loginname :any='';
  public appPages = [
    { title: 'Project', url: '/projects', icon: 'paper-plane' },
    // { title: 'LOGOUT', url: '/folder/Outbox', icon: 'log-out' },
    // { title: 'Favorites', url: '/folder/Favorites', icon: 'heart' },
    // { title: 'Archived', url: '/folder/Archived', icon: 'archive' },
    // { title: 'Trash', url: '/folder/Trash', icon: 'trash' },
    // { title: 'Spam', url: '/folder/Spam', icon: 'warning' },
  ];
  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor(
    private router:Router,
    private authService:AuthService,
    private projectService: ProjectService,
    private Activatedroute:ActivatedRoute,
    private CommonService: CommonService
  ) {
    this.login=localStorage.getItem('user');
    this.loginname=JSON.parse(this.login);

    console.log("THis is dgjn",this.loginname);
  }

  logout() {
    this.loading = false;
    this.authService.logout();
    // console.log("in my room work",this.userDetails);
   
}
}
