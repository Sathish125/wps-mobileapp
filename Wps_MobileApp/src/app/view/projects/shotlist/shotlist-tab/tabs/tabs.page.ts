import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  backicon(){
    console.log("this is second click");

    this.router.navigateByUrl('projects/shotlist/shotlisdetails');
  }
  opentab(){
    console.log("this is first click");
    this.router.navigateByUrl('projects/shotlist/tabs/tab2');
  }

}
