import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  shot_typelocal:any=[];

  constructor(
    private commonservice:CommonService
  ) { }

  ngOnInit() {
    this.shot_typelocal=this.commonservice.shot_type
    console.log("shot_type 456",this.commonservice.shot_type);
  }
 
}
