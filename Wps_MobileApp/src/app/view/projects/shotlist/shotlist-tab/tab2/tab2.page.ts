import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TakepopupComponent } from './takepopup/takepopup.component';
@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.page.html',
  styleUrls: ['./tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  data=[1,2,3,4,5];
  constructor(
    private modelpopup:ModalController
  ) { }

  ngOnInit() {
  }
  async openPopUp(hover){
    const model= await this.modelpopup.create({
      component:TakepopupComponent
    });
    await model.present();
  }
}
