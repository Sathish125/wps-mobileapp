import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-takepopup',
  templateUrl: './takepopup.component.html',
  styleUrls: ['./takepopup.component.scss'],
})
export class TakepopupComponent implements OnInit {
  take:Take;
  constructor(
    private modelservice:ModalController
  ) {
  
   }

  ngOnInit() {
    this.take=new Take();
  }
  popupdetails(){
    // this.take
    console.log("this is take details",this.take);
  }
  popupclose(){
    this.modelservice.dismiss();
  }

}
export class Take{
  takeno:any='';
  memory:any='';
  Reference:any='';
  notes:any='';
}
