import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';
@Component({
  selector: 'app-shotlist',
  templateUrl: './shotlist.page.html',
  styleUrls: ['./shotlist.page.scss'],
})
export class ShotlistPage implements OnInit {
  documentid:any='';
  documentId:any='';
  projectId:any='';
  documentName:any='';
  documentscens=[];
  // scenesList:any=[];

  constructor(
    private router:Router,
    private projectservice:ProjectService
    ) { }

  ngOnInit() {
   this.documentId=localStorage.getItem('documentId');
    this.projectId=localStorage.getItem('projectId');
    this.documentName=localStorage.getItem('documentName');
    
    console.log('local storage values project id',this.projectId);
    console.log('local storage values documenet id',this.documentId);
    this.fetchScenes();
   
  }
  shotlists(sluglineId,sceneId){
    console.log("this is scene Id",sluglineId,sceneId);
    localStorage.setItem("sceneId",sceneId);
    localStorage.setItem("sluglineId",sluglineId);
    this.router.navigate(['projects/shotlist/shotlisdetails']);
    
  }
  fetchScenes(){
    this.projectservice.fetchScenes(this.projectId,this.documentId)
    .subscribe(
      data =>
      {
        this.documentscens=data.scenesList;
        console.log("this is fetch scens",this.documentscens);
        Array.prototype.forEach.call(this.documentscens, child => {
          console.log("this is for each reference",child.scenesList)
        }); 
      }
    )
  }
  backicon(){
    this.router.navigate(['projects/documents'])
  }

 
}
