import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectsPage } from './projects.page';
// import{DocumentsComponent} from './documents/documents.component'

const routes: Routes = [
  {
    path: '',
    component: ProjectsPage
  },
  // {
  //   path:'documents',
  //   component:DocumentsComponent
  // },
  {
    path: 'documents',
    loadChildren: () => import('./documents/documents.module').then( m => m.DocumentsPageModule)
  },
  {
    path: 'shotlist',
    loadChildren: () => import('./shotlist/shotlist.module').then( m => m.ShotlistPageModule)
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectsPageRoutingModule {}
