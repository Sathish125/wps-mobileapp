import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
// import { ToastrService } from 'node_modules/process-nextick-args';
import { AuthService } from '../../services/auth.service';
import { ProjectService } from '../../services/project.service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.page.html',
  styleUrls: ['./projects.page.scss'],
})
export class ProjectsPage implements OnInit {

  moduleInnerLoading: boolean = false;
  projects: any[] = [];
  isRecordFound: boolean = false;
  project_id: string = null;
  document_id: string = null;
  userId: string = null;
  userDetails:any=[];

  constructor(
    private router:Router,
    private authService:AuthService,
    private projectService: ProjectService,
    private Activatedroute:ActivatedRoute,
    private CommonService: CommonService
  ) { }

  ngOnInit() {
    let localData=localStorage.getItem("user");
    // console.log("Printing User Session Details: ",localData);
    this.userDetails=JSON.parse(localData);
    localStorage.removeItem('projectId');
    localStorage.removeItem('documentId');
    this.Activatedroute.paramMap.subscribe(params => { 
      this.project_id = params.get('project_id'); 
      this.document_id = params.get('document_id');
      // console.log("its checking");
  })

  this.CommonService.currentProjectName = 'Maanadu';
    this.getAllProjects();
    
  }

  getAllProjects() {

    this.moduleInnerLoading = true;
    this.projectService.getMyProjects(this.userDetails)
    .subscribe(
      data => {

        if(data.success == true) {

          if(data.response.length > 0) {
            this.projects = data.response;
            this.isRecordFound = true;
          }
        }
        this.moduleInnerLoading = false;
        // console.log("Printing Projects: ", this.projects);
      }
    );
  }

  ProjectWritedocument (projectId,projectName) {
    this.router.navigate(['/projects/documents', { data: 'WritePlan' }]);
    localStorage.setItem('projectId', projectId);
    localStorage.setItem('projectName', projectName);
    this.fetchProjectDetails(projectId);
  }

  // FETCHING PROJECT NAME AND DETAILS 
  fetchProjectDetails(projectId) {
    this.projectService.fetchProjectById(projectId)
    .subscribe(
      data => {
        // console.log('Printing Project Details: ',data);
        if(data.success == true) {
          // if (data.response.length == 1) {
            let projectName = data.response.projectname;
            localStorage.setItem('projectName', projectName);
          // }
          // else{
            // No such Document available
          // }
          
        }
      }
    );
  }


  // logout
  logout() {
    this.authService.logout();
    // console.log("in my room work",this.userDetails);
   
}

}
